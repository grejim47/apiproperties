﻿using ApiProperties.Context;
using ApiProperties.Interfaces;
using ApiProperties.Models;
using ApiProperties.Models.Context;
using Microsoft.EntityFrameworkCore;

namespace ApiProperties.Implements
{
    public class PropertyImplements : IProperty
    {
        private readonly PropertiesContext _context;
        public PropertyImplements(PropertiesContext context)
        {
            _context = context;
        }

        public async Task<List<Property>> GetProperties()
        {
            try
            {
                return await _context.Property.ToListAsync();
                //return await _context.Property.Include(a => a.Owner).ToListAsync();
            }
            catch (Exception)
            {
                return new List<Property>();
            }
        }

        public async Task<Property> GetPropertieDetails(int idProperty)
        {
            try
            {
                //return await _context.Property.Include(a => a.Owner).Where(a => a.IdProperty == idProperty).FirstOrDefaultAsync();
                return await _context.Property.Where(a => a.IdProperty == idProperty).FirstOrDefaultAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<List<PropertyImage>> GetPropertieImages(int idProperty)
        {
            try
            {
                return await _context.PropertyImage.Where(a => a.IdProperty == idProperty).ToListAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<bool> AddImage(Imagen imagen)
        {
            try
            {
                PropertyImage newImg = new PropertyImage(imagen);
                await _context.PropertyImage.AddAsync(newImg);
                if (await _context.SaveChangesAsync() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ChangePrice(ChancePrice chancePrice)
        {
            try
            {
                var property = await _context.Property.Where(p => p.IdProperty == chancePrice.IdProperty).FirstOrDefaultAsync();
                if (property != null)
                {
                    property.Price = chancePrice.Price;
                    _context.Update(property);
                    await _context.SaveChangesAsync();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> CreateProperty(NewProperty NewProperty)
        {
            try
            {
                Owner owner = new Owner(NewProperty.NewOwner);
                await _context.Owner.AddAsync(owner);
                await _context.SaveChangesAsync();
                Property property = new Property(NewProperty, owner.IdOwner);
                await _context.Property.AddAsync(property);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
