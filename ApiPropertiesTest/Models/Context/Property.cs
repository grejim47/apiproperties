﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiProperties.Models.Context
{
    public class Property
    {
        [Key]
        public int IdProperty { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public decimal Price { get; set; }
        public int CodeInternal { get; set; }
        public int Year { get; set; }
        [Column("IdOwner")]
        public int IdOwner { get; set; }
        public Property()
        {

        }
        public Property(NewProperty newProperty, int idOwner) 
        {
            this.Name = newProperty.Name;
            this.Address = newProperty.Address;
            this.Price = newProperty.Price;
            this.CodeInternal = newProperty.CodeInternal;
            this.Year = newProperty.Year;
            this.IdOwner = idOwner;
        }
    }
}
