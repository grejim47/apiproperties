﻿using System.ComponentModel.DataAnnotations;

namespace ApiProperties.Models.Context
{
    public class PropertyImage
    {
        [Key]
        public int IdPropertyImage { get; set; }
        public int IdProperty { get; set; }
        public string Files { get; set; }
        public bool Enable { get; set; }
        public PropertyImage()
        {

        }
        public PropertyImage(Imagen imagen)
        {
            this.IdProperty = imagen.IdProperty;
            this.Files = imagen.File;
            this.Enable = true;
        }
    }
}
