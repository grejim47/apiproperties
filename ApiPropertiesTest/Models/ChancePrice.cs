﻿namespace ApiProperties.Models
{
    public class ChancePrice
    {
        public decimal Price { get; set; }
        public int IdProperty { get; set; }
    }
}
