﻿namespace ApiProperties.Models
{
    public class NewProperty
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public decimal Price { get; set; }
        public int CodeInternal { get; set; }
        public int Year { get; set; }

        public NewOwner NewOwner { get; set; }
    }
    public class NewOwner
    {
        public string Name { get; set;}
        public string Address { get; set; }
        public string Photo { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
